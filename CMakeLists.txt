cmake_minimum_required(VERSION 3.5)
project(knapp_evaluation)

# Default to C++17
if(NOT CMAKE_CXX_STANDARD)
  set(CMAKE_CXX_STANDARD 17)
endif()

if(CMAKE_COMPILER_IS_GNUCXX OR CMAKE_CXX_COMPILER_ID MATCHES "Clang")
  add_compile_options(-Wall -Wextra -Wpedantic -g)
endif()

# find dependencies
find_package(ament_cmake REQUIRED)
find_package(rclcpp REQUIRED)
find_package(rosidl_default_generators REQUIRED)
find_package(yaml-cpp REQUIRED)
find_package(geometry_msgs REQUIRED)
find_package(visualization_msgs REQUIRED)


set(SOURCES src/Order.cpp
            src/Part.cpp 
            src/Product.cpp
            src/QueueItem.cpp
            src/OrderOptimizer.cpp)

add_executable(order_optimizer src/main.cpp)

add_library(classes STATIC ${SOURCES})

target_include_directories(classes PUBLIC
$<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>
$<INSTALL_INTERFACE:include> 
)

ament_target_dependencies(classes rclcpp geometry_msgs visualization_msgs)
target_link_libraries(classes yaml-cpp)
ament_target_dependencies(order_optimizer rclcpp yaml-cpp geometry_msgs visualization_msgs)
target_link_libraries(order_optimizer ${YAML_CPP_LIBRARIES} classes)

install(TARGETS 
  order_optimizer
  classes
  DESTINATION lib/${PROJECT_NAME}
)

set(order_msg
  "msg/Order.msg"
)

rosidl_generate_interfaces(${PROJECT_NAME}
  ${order_msg}
)

rosidl_target_interfaces(classes
  ${PROJECT_NAME} "rosidl_typesupport_cpp"
)

ament_export_dependencies(rosidl_default_runtime)

install(
  DIRECTORY include
  DESTINATION include
)

ament_export_include_directories(
  include
)

if(BUILD_TESTING)
  find_package(ament_lint_auto REQUIRED)
  # the following line skips the linter which checks for copyrights
  # uncomment the line when a copyright and license is not present in all source files
  #set(ament_cmake_copyright_FOUND TRUE)
  # the following line skips cpplint (only works in a git repo)
  # uncomment the line when this package is not in a git repo
  #set(ament_cmake_cpplint_FOUND TRUE)
  ament_lint_auto_find_test_dependencies()
endif()

ament_package()
