#include "knapp_evaluation/QueueItem.hpp"

QueueItem::QueueItem(std::string partName, std::string productName, double distance)
    : partName_(partName), productName_(productName), distance_(distance){};

std::string QueueItem::getPartName()
{
    return partName_;
};
std::string QueueItem::getProductName()
{
    return productName_;
};
double QueueItem::getDistance()
{
    return distance_;
}
 