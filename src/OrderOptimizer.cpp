
#include "knapp_evaluation/OrderOptimizer.hpp"

#include <chrono>
#include <queue>
#include <filesystem>
#include <thread>
#include <future>
#include <algorithm>
#include <yaml-cpp/yaml.h>

using namespace std::placeholders;
using namespace std::chrono_literals;
namespace fs = std::filesystem;

OrderOptimizer::OrderOptimizer(std::string path) : Node("order_optimizer"), path_(path), orderFound_(false)
{
    getConfigurations(path + "/configuration");
    nextOrderSub_ = this->create_subscription<knapp_evaluation::msg::Order>(
        "nextOrder", 10, std::bind(&OrderOptimizer::getOrders, this, _1));

    currentPositionSub_ = this->create_subscription<geometry_msgs::msg::PoseStamped>(
        "currentPosition", 1, std::bind(&OrderOptimizer::updatePosition, this, _1));

    publisher_ = this->create_publisher<visualization_msgs::msg::MarkerArray>(
        "order_path", 10);

    timer_ = this->create_wall_timer(500ms, std::bind(
                                                &OrderOptimizer::publishMarkers, this));
};

void OrderOptimizer::publishMarkers()
{
    visualization_msgs::msg::MarkerArray markerArray;
    visualization_msgs::msg::Marker marker;
    rclcpp::Time stamp = this->get_clock()->now();

    // Set marker for the AMR
    marker.header.frame_id = "world";
    marker.header.stamp = stamp;
    marker.ns = "AMR";
    marker.id = 0;

    marker.type = visualization_msgs::msg::Marker::CUBE;

    marker.action = visualization_msgs::msg::Marker::ADD;

    marker.pose.position.x = currentPosition_.pose.position.x;
    marker.pose.position.y = currentPosition_.pose.position.y;

    marker.scale.x = 1.0;
    marker.scale.y = 1.0;
    marker.scale.z = 1.0;

    markerArray.markers.push_back(marker);

    for (int i = 0; i < partQueue_.size(); i++)
    {

        visualization_msgs::msg::Marker partMarker;
        partMarker.header.frame_id = "world";
        partMarker.header.stamp = stamp;
        partMarker.ns = "Part";
        partMarker.id = i + 1;

        partMarker.type = visualization_msgs::msg::Marker::CYLINDER;

        partMarker.action = visualization_msgs::msg::Marker::ADD;

        partMarker.pose.position.x = parts_[partQueue_[i]->getPartName()]->getCx();
        partMarker.pose.position.y = parts_[partQueue_[i]->getPartName()]->getCy();
        
        partMarker.scale.x = 1.0;
        partMarker.scale.y = 1.0;
        partMarker.scale.z = 2.0;

        markerArray.markers.push_back(partMarker);
    }

    publisher_->publish(markerArray);
}

void OrderOptimizer::getOrders(const knapp_evaluation::msg::Order::SharedPtr msg)
{
    partQueue_.clear();
    orderFound_ = false;

    fs::path dataFolder = path_ + "/orders";

    std::vector<std::future<void>> futures;

    for (auto const &file : fs::directory_iterator{dataFolder})
    {
        std::future<void> f = std::async(std::launch::async, std::bind(&OrderOptimizer::parseOrders, this, file.path(), std::to_string(msg->order_id)));
        futures.push_back(std::move(f));
    }

    for (std::vector<std::future<void>>::iterator it = futures.begin(); it != futures.end(); it++)
    {
        it->wait();
    }


    if (order_ == nullptr)
    {
        RCLCPP_ERROR(this->get_logger(), "Order %d details were not found.", msg->order_id);
        return;
    }

    std::vector<std::shared_ptr<IProduct>> products = order_->getOrderProducts();

    arrangeByDistance(products);

    RCLCPP_INFO(this->get_logger(), "Working on order %d.", msg->order_id);

    for (size_t i = 0; i < partQueue_.size(); i++)
    {
        RCLCPP_INFO(this->get_logger(), "%d. Fetching part '%s' for product '%s' at"
                                        "x: %g, y: %g",
                    i + 1, partQueue_[i]->getPartName().c_str(), partQueue_[i]->getProductName().c_str(),
                    parts_[partQueue_[i]->getPartName()]->getCx(),
                    parts_[partQueue_[i]->getPartName()]->getCy());
    }

    RCLCPP_INFO(this->get_logger(), "%d. Delivering to destination x: '%g', y: '%g'",
                partQueue_.size() + 1,
                order_->getCx(),
                order_->getCy());
};

void OrderOptimizer::arrangeByDistance(std::vector<std::shared_ptr<IProduct>> &products)
{
    double cx = currentPosition_.pose.position.x;
    double cy = currentPosition_.pose.position.y;

    std::map<std::pair<std::string, std::string>, double> distances;

    for (size_t i = 0; i < products.size(); i++)
    {

        for (size_t j = 0; j < products[i]->getParts().size(); j++)
        {

            double distance = sqrt(
                pow(abs(cx - products[i]->getParts()[j]->getCx()), 2) +
                pow(abs(cy - products[i]->getParts()[j]->getCy()), 2));

            partQueue_.push_back(std::make_shared<QueueItem>(
                products[i]->getParts()[j]->getName(),
                products[i]->getName(),
                distance));
        }
    }

    std::sort(partQueue_.begin(), partQueue_.end(), [](std::shared_ptr<QueueItem> a, std::shared_ptr<QueueItem> b)
              { return a->getDistance() < b->getDistance(); });
}

void OrderOptimizer::updatePosition(const geometry_msgs::msg::PoseStamped::SharedPtr msg)
{
    currentPosition_ = *msg;
}

void OrderOptimizer::parseOrders(std::string path, std::string orderId)
{
    YAML::Node sequence = YAML::LoadFile(path);
    for (auto orders : sequence)
    {
        std::stringstream ss;
        ss << orders["order"];
        std::string orderName(ss.str());

        if (orderName == orderId)
        {
            orderFound_ = true;

            ss.str(std::string());
            ss << orders["cx"];
            double cx(atof(ss.str().c_str()));

            ss.str(std::string());
            ss << orders["cy"];
            double cy(atof(ss.str().c_str()));

            std::vector<std::shared_ptr<IProduct>> products;
            for (auto product : orders["products"])
            {
                ss.str(std::string());
                ss << product;
                uint productId(atoi(ss.str().c_str()));

                size_t val = this->products_.count(productId);
                if (this->products_.count(productId))
                {
                    products.push_back(this->products_[productId]);
                }
                else
                {
                    RCLCPP_WARN(
                        this->get_logger(),
                        "Product %d in order %s does not exist in configuration.",
                        productId, orderName.c_str());
                }
                std::lock_guard<std::mutex> lock(mutex_);
                order_ = std::make_shared<Order>(orderName, products, cx, cy);
            }
        }
        else if (orderFound_)
        {
            return;
        }
    }
};

void OrderOptimizer::getConfigurations(std::string path)
{
    fs::path dataFolder = path;
    for (auto const &file : fs::directory_iterator{dataFolder})
    {
        YAML::Node sequence = YAML::LoadFile(file.path());

        for (auto products : sequence)
        {
            std::stringstream ss;
            ss << products["id"];
            uint productId(atoi(ss.str().c_str()));

            ss.str(std::string());
            ss << products["product"];
            std::string productName(ss.str());

            if (!products_.count(productId))
            {
                products_[productId] = std::make_shared<Product>(productId, productName);

                for (auto parts : products["parts"])
                {
                    ss.str(std::string());
                    ss << parts["part"];
                    std::string partName(ss.str());

                    if (!parts_.count(partName))
                    {

                        ss.str(std::string());
                        ss << parts["cx"];
                        double cx(atof(ss.str().c_str()));

                        ss.str(std::string());
                        ss << parts["cy"];
                        double cy(atof(ss.str().c_str()));

                        parts_[partName] = std::make_shared<Part>(partName, cx, cy);
                    }

                    products_[productId]->addPart(parts_[partName]);
                }
            }
        }
    }
};