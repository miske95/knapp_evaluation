#include "rclcpp/rclcpp.hpp"
#include "knapp_evaluation/OrderOptimizer.hpp"
#include <filesystem>

namespace fs = std::filesystem;

bool checkParameterValidity(std::string path){

    std::stringstream ss;
    ss << path << "/orders";

    fs::path dataFolder = ss.str();
    if(!fs::exists(dataFolder)) return 0;

    ss.str(std::string());
    
    ss << path << "/configuration";
    dataFolder = ss.str();

    if(!fs::exists(dataFolder)) return 0;

    return 1;
}

int main(int argc, char **argv){

    auto logger = rclcpp::get_logger("logger");

    if (argc != 2)
    {
        RCLCPP_INFO(
            logger, "Invalid number of parameters\nPath to orders and configuration "
            "must be provided.");
        return 1;
    }
    
    if(!checkParameterValidity(argv[1]))
    {
        RCLCPP_ERROR(logger, "Path to folder not valid.");
        return 1;
    }

    rclcpp::init(argc, argv);
    rclcpp::spin(std::make_shared<OrderOptimizer>(argv[1]));
    rclcpp::shutdown();

    return 0;
}