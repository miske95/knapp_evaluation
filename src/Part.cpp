#include <knapp_evaluation/Part.hpp>

Part::Part(std::string name, double cx, double cy)
    : name_(name), cx_(cx), cy_(cy){};

std::string Part::getName() const
{
    return name_;
}
double Part::getCx() const
{
    return cx_;
}
double Part::getCy() const
{
    return cy_;
}
