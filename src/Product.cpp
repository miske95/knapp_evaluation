#include "knapp_evaluation/Product.hpp"

Product::Product(uint id, std::string name)
    : id_(id), name_(name) {}

void Product::addPart(std::shared_ptr<IPart> part)
{
    parts_.push_back(part);
}

uint Product::getId()
{
    return id_;
}

std::string Product::getName()
{
    return name_;
}

std::vector<std::shared_ptr<IPart>> Product::getParts()
{
    return parts_;
}
