#include "knapp_evaluation/Order.hpp"
#include "knapp_evaluation/IProduct.hpp"

Order::Order(std::string id, std::vector<std::shared_ptr<IProduct>> products, double cx, double cy)
    : id_(id), products_(products), cx_(cx), cy_(cy) {}

void Order::setCoordinates(double x, double y)
{
    cx_ = x;
    cy_ = y;
}

std::string Order::getID() const
{
    return id_;
}
double Order::getCx() const
{
    return cx_;
}
double Order::getCy() const
{
    return cy_;
}
std::vector<std::shared_ptr<IProduct>> Order::getOrderProducts() const
{
    return products_;
}
