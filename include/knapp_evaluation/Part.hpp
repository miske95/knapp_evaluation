#ifndef PART_HPP
#define PART_HPP

#include <cstdlib>
#include <string>
#include <knapp_evaluation/IPart.hpp>

class Part : public IPart
{
public:
    Part(std::string name, double cx, double cy);

    std::string getName() const override;
    double getCx() const override;
    double getCy() const override;

private:
    std::string name_;
    double cx_;
    double cy_;
};

#endif