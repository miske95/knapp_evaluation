#ifndef PRODUCT_HPP
#define PRODUCT_HPP

#include <cstdlib>
#include <vector>
#include <memory>
#include "knapp_evaluation/IProduct.hpp"
#include <knapp_evaluation/IPart.hpp>
 
class Product : public IProduct
{
    public:
    Product(uint id, std::string name);

    void addPart(std::shared_ptr<IPart> part) override;
    virtual uint getId() override;
    std::string getName() override;
    std::vector<std::shared_ptr<IPart>> getParts() override;

    private:
    uint id_;
    std::string name_;
    std::vector<std::shared_ptr<IPart>> parts_;
};

#endif