#ifndef QUEUEITEM_HPP
#define QUEUEITEM_HPP

#include <cstdlib>
#include <vector>
#include <memory>
#include "knapp_evaluation/IProduct.hpp"
#include <knapp_evaluation/IPart.hpp>

class QueueItem
{
public:
    QueueItem(std::string partName, std::string productName, double distance);

    std::string getPartName();
    std::string getProductName();
    double getDistance();

private:
    std::string partName_;
    std::string productName_;
    double distance_;
};

#endif