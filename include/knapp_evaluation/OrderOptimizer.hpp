#ifndef ORDER_OPTIMIZER_HPP
#define ORDER_OPTIMIZER_HPP

#include "rclcpp/rclcpp.hpp"
#include "geometry_msgs/msg/pose_stamped.hpp"
#include "visualization_msgs/msg/marker_array.hpp"
#include "knapp_evaluation/msg/order.hpp"
#include "knapp_evaluation/Product.hpp"
#include "knapp_evaluation/Part.hpp"
#include "knapp_evaluation/Order.hpp"
#include "knapp_evaluation/IProduct.hpp"
#include "knapp_evaluation/IPart.hpp"
#include "knapp_evaluation/IOrder.hpp"
#include "knapp_evaluation/QueueItem.hpp"

#include <memory>
#include <mutex>
  
using namespace std::placeholders;
using namespace std::chrono_literals;
  
class OrderOptimizer : public rclcpp::Node
{
public:
    explicit OrderOptimizer(std::string path);
    void publishMarkers();
    void getOrders(const knapp_evaluation::msg::Order::SharedPtr msg);
    void arrangeByDistance(std::vector<std::shared_ptr<IProduct>> &products);
    void updatePosition(const geometry_msgs::msg::PoseStamped::SharedPtr msg);
    void parseOrders(std::string path, std::string orderId);
    void getConfigurations(std::string path);

private:
    rclcpp::Subscription<geometry_msgs::msg::PoseStamped>::SharedPtr currentPositionSub_;
    rclcpp::Subscription<knapp_evaluation::msg::Order>::SharedPtr nextOrderSub_;
    rclcpp::Publisher<visualization_msgs::msg::MarkerArray>::SharedPtr publisher_;
    rclcpp::TimerBase::SharedPtr timer_;
    std::map<std::string, std::shared_ptr<IPart>> parts_;
    std::map<uint, std::shared_ptr<IProduct>> products_;
    std::shared_ptr<IOrder> order_;
    std::string path_;
    geometry_msgs::msg::PoseStamped currentPosition_;
    std::vector<std::shared_ptr<QueueItem>> partQueue_;
    std::mutex mutex_;
    bool orderFound_;
};

#endif
