#ifndef IPART_HPP
#define IPART_HPP

#include <cstdlib>
#include <string>

class IPart
{
public:
    virtual ~IPart(){};
    virtual std::string getName() const = 0;
    virtual double getCx() const = 0;
    virtual double getCy() const = 0;
};

#endif