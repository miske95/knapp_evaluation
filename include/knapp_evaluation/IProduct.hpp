#ifndef IPRODUCT_HPP
#define IPRODUCT_HPP

#include <cstdlib>
#include <vector>
#include <memory>
#include "knapp_evaluation/IPart.hpp"
 
class IProduct
{
    public:
    virtual ~IProduct(){};
    virtual void addPart(std::shared_ptr<IPart> part) = 0;
    virtual uint getId() = 0;
    virtual std::string getName() = 0;
    virtual std::vector<std::shared_ptr<IPart>> getParts() = 0;

};

#endif