#ifndef ORDER_HPP
#define ORDER_HPP

#include <cstdlib>
#include <string>
#include <memory>
#include "knapp_evaluation/IOrder.hpp"
#include "knapp_evaluation/IProduct.hpp"

class Order : public IOrder
{
public:
    Order(std::string id, std::vector<std::shared_ptr<IProduct>> products, double cx, double cy);
    void setCoordinates(double x, double y) override;
    std::string getID() const override;
    double getCx() const override;
    double getCy() const override;
    std::vector<std::shared_ptr<IProduct>> getOrderProducts() const override;

private:
    std::string id_;
    std::vector<std::shared_ptr<IProduct>> products_;
    double cx_;
    double cy_;
};

#endif