#ifndef IORDER_HPP
#define IORDER_HPP

#include <cstdlib>
#include <string>
#include <memory>
#include "knapp_evaluation/IProduct.hpp"

class IOrder
{
public:
    virtual ~IOrder(){};
    virtual std::string getID() const = 0;
    virtual void setCoordinates(double x, double y) = 0;
    // virtual void setProducts(std::vector<std::shared_ptr<IProduct>> products)  = 0;
    virtual double getCx() const = 0;
    virtual double getCy() const = 0;
    virtual std::vector<std::shared_ptr<IProduct>> getOrderProducts() const = 0;
};

#endif